# COMP6001 - Assessment 2
## The Learning and Demonstration of GUI practice
## Assessment Journal

30/8/17 
James and I discussed potential project ideas and decided to use a cryptocurrency API. We then discussed the potential features and layout of our website and started to hand draw wireframes for potential designs.

31/8/17
James and I converted the hand drawn wireframes into digital designs and commenced work on the report. We had a basic draft detailing the information each person in our group would work on, the potential applications used in design and the additional resources we would use.

6/9/2017
James and I began assemblying a more detailed draft, supplying more information about our sections, applications and resources.

7/9/2017
We showed Jeff a couple of versions of our draft that still required further information. We continued to add more information and James turned our detailed draft into a final design plan. We are now ready to commence work on our project.